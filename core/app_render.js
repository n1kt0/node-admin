// imports
var exphbs  = require('express-handlebars');

function app_render(name, options, fn) {
    'use strict';
    var opts = {};
    var cache = this.cache;
    var engines = {'.hbs': exphbs(/*{defaultLayout: 'index'}*/)}; // here we patch our handlebars lib. object's key must equal to template extension put on line 44 below
    var view;
    
    function mixin(base, source) {
        if (base && source) {
            for (var key in source) {
                base[key] = source[key];
            }
        }
        return base;
    }
    
    // support callback function as second arg
    if ('function' == typeof options) {
            fn = options, options = {};
    }
        // merge app.locals
    mixin(opts, this.locals);

    // merge options._locals
    if (options._locals) mixin(opts, options._locals);

    // merge options
    mixin(opts, options);

    // set .cache unless explicitly provided
    opts.cache = null == opts.cache
        ? this.enabled('view cache')
        : opts.cache;

    // primed cache
    if (opts.cache) view = cache[name];

    // view
    if (!view) {
        view = new (this.get('view'))(name, {
            defaultEngine: 'hbs', // here we patch our handlebars templates extension. Comes from app.set('view engine', '[...]') command
            root: __dirname + '/views', // here we patch the views dir
            engines: engines
        });

        if (!view.path) {
            var err = new Error('Failed to lookup view "' + name + '" in views directory "' + view.root + '"');
            err.view = view;
            return fn(err);
        }

        // prime the cache
        if (opts.cache) cache[name] = view;
    }

    // render
    try {
        view.render(opts, fn);
    } catch (err) {
        fn(err);
    }
}

module.exports = app_render