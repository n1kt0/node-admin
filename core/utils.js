'use strict';

// imports
var hash        = require('./pass');
    
module.exports = {
    authenticate: function (username, pwd, cb) {
        this.db.collection('admin_mod-users', function (err, users) {
            if (err) return cb(err);
            users.findOne({name: username}, function (err, user) {
                if (err || user === null) return cb(err || new Error('User not found')); // user not found
                hash(pwd, user.salt, function (err, hash) {
                    if (err) return cb(err);
                    if (hash === user.hash) return cb(err, user);
                    cb(new Error('invalid password'));
                });
            });
        });
    }
};