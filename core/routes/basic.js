// Admin module routes (not API-related)
'use strict';

// imports
var render    = require('../res_render'),
    auth      = require('../utils').authenticate,
    restrict  = require('../middlewares/restrict'),
    dbUtils   = require('../db');

module.exports = function (router) {
    var self = this; // cache the app context
    
    // Main page
    router.get('/', restrict.bind(self), function (req, res) {
        res.locals = {
            user: req.session.user,
            adminUrl: self.url
        }
        render.call(res, 'index'); // overriding Express's render() func 
    });
    
    // Login page
    router.route('/login')
    .get(function (req, res) {
         res.locals = {
            adminUrl: self.url
        }
        render.call(res, 'login');
    })
    .post(function (req, res) {
        var userName = req.body.username, password = req.body.password;
        
        auth.call(self, userName, password, function (err, user) {
            if (user) {
                req.session.regenerate(function () {
                   req.session.user = {
                       name: user.name,
                       id: user._id
                   }
                   res.redirect(self.url);
                });
            } else {
                req.session.error = "Authentication failed. Double check login/password."
                res.redirect(self.url + '/login');
            }
        });
    });
    
    // Logout url
    router.get('/logout', function (req, res) {
        req.session.destroy(function () {
            res.redirect(self.url);
        })
    })
}