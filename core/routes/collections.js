// API routes: master app collections
'use strict';

// IMPORTS
// local
var render          = require('../res_render'),
    restrictDefault = require('../middlewares/restrict');

module.exports = function (router) {
    var self        = this, // cache the app context
        restrict    = restrictDefault.bind(self); // adopt restrict middleware
    
    /*Router params*/
    
    // cache 'collection_name' in req object
    router.param('collection_name', function (req, res, next, collection) {
        req.collectionName = collection;
        next();
    });
    
    /*Routes*/
    
    router.route('/collections')
    // get the list of all collections except of MongoDB's service ones
    .get(restrict, function (req, res) {
        self.db.listCollections().toArrayAsync()
        .then(function (collections) {
            var collections = collections
                .filter(function (collection) {
                return collection.name.search(/.indexes/) === -1;
                })
                .map(function (collection) {
                    collection.name = collection.name.replace(/^[a-zA-Z0-9]+\./, '');
                    return collection;
                });
            res.status(200).json(collections);
        })
        .catch(function (e) {
            console.log(e.toString());
            res.redirect(back);
        });
    });

        
    // collection API
    router.route('/collections/:collection_name')
    // fetch specified collection docs from DB.
    .get(restrict, function (req, res) {
        
        self.db.collectionAsync(req.collectionName, {strict: true})
        .then(function (collection) {
            return collection.find({}, {fields: {salt:0, hash:0}});
        })
        .then(function (cursor) {
            return cursor.toArrayAsync();
        })
        .then(function (docs) {
            res.status(200).json(docs);
        })
        .catch(function (e) {
            console.log(e.toString());
            res.status(404).send('Not Found');
        });
    })
    
    // add new collection to DB. Since collections don't have ID attribute their creation is done with PUT request (name == ID in collection's model)
    .put(restrict, function (req, res) {
        
        self.db.createCollectionAsync(req.collectionName, {strict: true})
        .then(function (collection) {
            res.status(201).send('Collection Created!')
            return collection;
        })
        .catch(function (e) {
            console.log(e.toString());
            res.status(400).send('Error:', e.toString());
        });
    })

    // delete collection
    .delete(restrict, function (req, res) {

        self.db.dropCollectionAsync(req.collectionName)
        .then(function (result) {
            res.status(204).send(result);
        })
        .catch(function (e) {
            console.log(e.toString());
            res.status(400).send('Error:', e.toString());
        })
    });
    
    // Collection Doc API
    
    router.route('/collections/:collection_name/:doc_id')
    // fetch specific doc of a specific collection
    .get(restrict, function (req, res) {
        var docID = req.params.doc_id;
        
        if (docID.toString().length < 12) {
            console.log('Wrong ID for collection was supllied!');
            return res.status(404).send('Not Found');
        }
        
        self.db.collectionAsync(req.collectionName, {strict: true})
        .then(function (collection) {
            return collection.findOneAsync({_id: docID});
        })
        .then(function (doc) {
            if (doc === null) {
                return res.status(404).send('Not Found');
            };
            res.status(200).json(doc);
        })
        .catch(function (e) {
            console.log(e.toString());
            res.status(404).send('Not Found');
        });
    })
    // doc update
    .put(restrict, function (req, res) {
        var docID    = req.params.doc_id,
            docData  = req.body;

        self.db.collectionAsync(req.collectionName, {strict: true})
        .then(function (collection) {
            return collection.findAndModifyAsync({_id: docID}, [['_id', 1]],  docData, {w:1})
        })
        .then(function () {
            res.status(201).send('Doc wsa updated successfully!')
        })
        .catch(function (e) {
            console.log(e.toString());
            res.status(400).send('Error:', e.toString());
        });
    })
    
}