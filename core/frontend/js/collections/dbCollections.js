'use strict';

var AmpersandCollection = require('ampersand-rest-collection'),
    dbCollection = require('../models/dbCollection');

module.exports = AmpersandCollection.extend({
    url: 'collections',
    model: dbCollection,
    comparator: 'name'
});