'use strict';
/*global app */

// global imports
var Router = require('ampersand-router'),
// internal imports
    DashboardView       = require('./views/main/modules/dashboard/dashboardModuleView'),
    DbModuleView        = require('./views/main/modules/db/dbModuleView'),
    UsersModuleView     = require('./views/main/modules/users/usersModuleView'),
    SettingsModuleView  = require('./views/main/modules/settings/settingsModuleView'),
    CollectionView      = require('./views/main/modules/db/collectionView');


module.exports = Router.extend({
    routes: {
        'dashboard': 'dashboard',
        'settings': 'settings',
        'users': 'users',
        'db': 'db',
        'db/*collectionName': 'openCollection',
        '*default': 'defaultRoute'
    },
    
    modulesViews: {
            dashboard: DashboardView,
            settings: SettingsModuleView,
            users: UsersModuleView,
            db: DbModuleView
    },
    
    switchModule: function (moduleName) {
        var view = this.modulesViews[moduleName],
            moduleView = new view({
            el: document.createElement('div')
            });
        
        admin_app.view.contentSwitcher.set(moduleView);
    },
    
    defaultRoute: function () {
        this.switchModule('dashboard');
    },
    
    dashboard: function () {
        this.switchModule('dashboard');
    },
    
    settings: function () {
        this.switchModule('settings');
    },
    
    users: function () {
        this.switchModule('users');
    },
    
    db: function () {
        this.switchModule('db');
    },
    
    openCollection: function (collectionName) {
        var view = new CollectionView({
            el: document.createElement('div'),
            collection: admin_app.me.get(collectionName + 'Collection'),
            collectionTitle: collectionName
        });
        admin_app.view.contentSwitcher.set(view);
    }
    
});