'use strict';

var AmpersandModel = require('ampersand-model');

module.exports = AmpersandModel.extend({
    idAttribute: 'name',
    //urlRoot: 'collections',
    props: {
        name: 'string'
    }
});