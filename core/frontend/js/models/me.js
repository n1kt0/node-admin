'use strict';

var AmpersandState      = require('ampersand-state'),
    AmpersandModel      = require('ampersand-model'),
    AmpersandCollection = require('ampersand-rest-collection'),
    DbCollections       = require('../collections/dbCollections');

module.exports = AmpersandState.extend({
    extraProperties: 'allow', // because we don't know in advance which collections will be gotten from DB
    collections: {
        dbCollections: DbCollections
    },
    initialize: function () {
        this.dbCollections.on('add', this.createCollection.bind(this)); // overriding default collection context
    },
    createCollection: function (dbCollection) {
        var dbCollectionName = dbCollection.get('name');

        var dbCollectionDocModel = AmpersandModel.extend({
            idAttribute: '_id',
            
            extraProperties: 'allow',
            
            // the only prop we know in advance is _id attr
            props: {
                _id: 'string'
            },
            
            initialize: function () {
            // update model with attrs received from server. Ampersand doesn't add attrs if they are not descrbed in model, need to add them manually
                this.on('add', function (model, collection, xhrObject) {
                    var modelID = model.get('_id'),
                        serverModels = JSON.parse(xhrObject.xhr.body);
                    // find our model data in JSON from server
                    for (var i = 0, l = serverModels.length; i < l; i++) {
                        if (serverModels[i]._id === modelID) {
                            // update model with this data
                            model.set(serverModels[i]);
                        };
                    };
                });
            },
            
            toArray: function () {
                var modelObject = this.toJSON(),
                    result = [];
                   
                for (var key in modelObject) {
                    result.push(modelObject[key]);
                }
                    
                return result;
            },
            
            getKeys: function () {
                var modelObject = this.toJSON(),
                    result = [];
                
                for (var key in modelObject) {
                    result.push(key);
                }
                
                return result;
            }
        });

        var dbCollectionCollection = AmpersandCollection.extend({
            url: 'collections/' + dbCollectionName,
            dataField: 'data',
            model: dbCollectionDocModel,
            initialize: function () {
                this.keys = []; // keys (attr names) of models this collection will store
                this.once('add', this.saveModelKeys, this);
            },
            
            saveModelKeys: function (model) {
                if (this.keys.length === 0) {
                    this.keys = model.getKeys();
                };
            }
        });
        
        this.set(dbCollectionName + 'Collection', new dbCollectionCollection());
    }
});