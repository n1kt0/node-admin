'use strict';

// global imports
var AmpersandView = require('ampersand-view');

module.exports = AmpersandView.extend({
    template: require('../../templates/side/module-list.hbs'),
    
    initialize: function () {
        this.render();
    },
    
    render: function () {
        this.el.innerHTML = this.template();
        return this;
    }
});