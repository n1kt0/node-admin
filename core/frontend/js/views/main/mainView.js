'use strict';
// global imports
var AmpersandView   = require('ampersand-view'),
	ViewSwitcher    = require('ampersand-view-switcher'),
// internal imports
    ModuleListView  = require('../side/moduleListView'),
    DashboardView   = require('./modules/dashboard/dashboardModuleView');

module.exports = AmpersandView.extend({
    initialize: function () {
        var self = this;
    	
        // get in the side pannel
        this.renderSubview(new ModuleListView({
            el: document.body.querySelector('[data-hook=module-list]')
        }), '[data-hook=side-block]');
        
        // establish switcher for the main content area
        // and render default view
        var mainContainer = this.queryByHook('main-block'),
            defaultView = new DashboardView({el: document.createElement('div')});
        
        this.contentSwitcher = new ViewSwitcher(mainContainer);
        this.contentSwitcher.set(defaultView);
        
        // get DB collections
        this.model.dbCollections.fetch();
    }
});