'use strict';

var AmpersandView = require('ampersand-view');

module.exports = AmpersandView.extend({
    template: require('../../../../templates/main/modules/settings/settings-module.hbs'),
    
    initialize: function () {
        this.render();
    },
    
    render: function () {
        this.el.innerHTML = this.template();
        return this;
    }
});