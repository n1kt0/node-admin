// GLOBAL APP

'use strict';
// global imports
var AmpersandView = require('ampersand-view'),
// local imports
    CollectionDocView = require('./collectionDocView'),
    CollectionTableHeaderView = require('./collectionTableHeaderView');
    
module.exports = AmpersandView.extend({
    template: require('../../../../templates/main/modules/db/collection.hbs'),
    
    initialize: function (options) {
        // cache collection title
        this.title = options.collectionTitle;
        // fetch collection from server
        this.collection.fetch();
        // render the view
        this.render();
    },
            
    render: function () {
        var self = this; // caching context
        
        // render master view
        this.el.innerHTML = this.template({title: this.title});
        
        // render fetched docs
        var docsCollectionView = this.renderCollection(this.collection, CollectionDocView, this.el.querySelector('[data-hook=collection-docs]'));
        
        docsCollectionView.collection.once('sync', function () {
            var self = this;
            // render table header subview
            var docsCollectionHeader = this.renderSubview(new CollectionTableHeaderView({
                el: document.createElement('thead'),
                keys: self.collection.keys
            }), '[data-hook=collection-content]');
        }, this);
        
        return this;
    }
});