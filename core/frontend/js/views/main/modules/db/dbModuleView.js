'use strict';

// global imports

var AmpersandView = require('ampersand-view'),
    CollectionItemView = require('./collectionItemView');

module.exports = AmpersandView.extend({
    template: require('../../../../templates/main/modules/db/db-module.hbs'),
    
    events: {
        'keypress [data-hook=new-collection-title]': 'createCollection',
        'click [data-hook=undo]': 'cancelCollectionDeletion'
    },
    
    initialize: function () {
        this.timer = {}; // here will be the el and timer ID passed from the child collection view when user tries to delete it
        this.render();
    },
    
    render: function () {
        this.el.innerHTML = this.template();
        this.renderCollection(admin_app.me.dbCollections, CollectionItemView, this.queryByHook('collections-list'), {filter: function (model) {
            return !/^admin_mod/.test(model.get('name'));
        }});
        return this;
    },
    
    createCollection: function (e) {
        var val = this.queryByHook('new-collection-title').value,
            newCollection;
        
        if (e.which === 13 && val.length > 3) {
            newCollection = val.trim().replace(/ /g, '-');
            admin_app.me.dbCollections.create({name: newCollection});
            this.queryByHook('new-collection-title').value = '';
        }
    },
    cancelCollectionDeletion: function () {
        clearTimeout(this.timer.timer);
        this.queryByHook('notification').style.opacity = 0;
        this.timer.el.classList.remove('hidden');
    }
});