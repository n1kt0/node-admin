'use strict';

// global imports
var AmpersandView = require('ampersand-view');

module.exports = AmpersandView.extend({
    template: require('../../../../templates/main/modules/db/collection-table-header.hbs'),
    
    initialize: function (options) {
        this.keysList = options.keys;
        this.render();
    },
    
    render: function () {
        var self = this;
        this.el.innerHTML = this.template({keys: self.keysList});
        return this;
    }
});