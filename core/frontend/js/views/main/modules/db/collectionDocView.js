'use strict';
//global imports
var AmpersandView = require('ampersand-view');

module.exports = AmpersandView.extend({
    template: require('../../../../templates/main/modules/db/collection-doc.hbs'),
    
    events: {
        'dblclick [db-field]': 'editValue',
        'blur input': 'updateValue'
      //  'keypress input': 'updateOnEnter'
    },
    
    initialize: function () {
        this.listenTo(this.model, 'change', this.render);
        this.render();
    },
    
    render: function () {
        this.renderWithTemplate(this.model.toJSON());
    },
    
    editValue: function (e) {
        var field       = e.target,
            fieldName   = field.attributes['db-field'].value,
            input       = this.queryByHook(fieldName);
        
        // do not process _id field
        if (fieldName === '_id') {
            return input.parentNode.removeChild(input);
        }
        
        // show input field
        field.classList.toggle('hidden');
        input.classList.toggle('hidden');
        input.focus();
    },
    
    updateValue: function (e) {
        var input     = e.target,
            inputName = input.attributes['data-hook'].value,
            field     = this.queryByHook(inputName + '-field');
        
        // process only non-empty input and new value
        if (input.value.length > 0 && input.value !== field.innerHTML) {
            return this.model.save(inputName, input.value);
        }
        
        // restore input placehoder with original value
        if (input.value.length === 0) {
            input.value = field.innerHTML;
        }
        
        // hide input field
        input.classList.toggle('hidden');
        field.classList.toggle('hidden');
    }
});