'use strict';

// global imports
var AmpersandView   = require('ampersand-view');


module.exports = AmpersandView.extend({
    template: require('../../../../templates/main/modules/db/collection-item.hbs'),
    events: {
        'click [data-hook=collection-delete]': 'deleteCollection'
    },
    bindings: {
        'model.name': [
            {
                type: 'text',
                hook: 'collection-title'
            },
            {
                type: function (el, value, prevValue) {
                    el.setAttribute('href', '#db/' + value);
                },
                hook: 'collection-open'
            }
        ]
    },
    initialize: function () {
        this.render();
    },
    render: function () {
        this.renderWithTemplate();
        return this;
    },
    deleteCollection: function () {
        var self        = this,
            notifier    = this.parent.parent.queryByHook('notification');
        notifier.style.opacity = 1;
        this.el.classList.add('hidden');
        var deletion = setTimeout(function () {
            self.model.destroy();
            self.remove();
            notifier.style.opacity = 0;
        }, 3000);
        this.parent.parent.timer = {el: this.el, timer: deletion}; // pass the el and timer to parent where cancellation will be clickable
    }
});