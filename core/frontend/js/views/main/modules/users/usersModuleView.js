'use strict';

var AmpersandView = require('ampersand-view');

module.exports = AmpersandView.extend({
    template: require('../../../../templates/main/modules/users/users-module.hbs'),
    
    initialize: function () {
        this.render();
    },
    
    render: function () {
        this.el.innerHTML = this.template();
        return this;
    }
});