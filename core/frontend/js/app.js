'use strict';
// global imports
var AmpersandCollection = require('ampersand-rest-collection'),
    AmpersandModel      = require('ampersand-model'),
// internal imports
    MainView            = require('./views/main/mainView'),
    Me                  = require('./models/me'),
    Router              = require('./router');

window.admin_app = {
    init: function () {
        this.me = new Me();
        this.view = new MainView({
            el: document.body,
            model: this.me
        });
        this.router = new Router();
        this.router.history.start();
    }
};

window.admin_app.init();