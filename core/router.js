// Admin module routes
'use strict';

// imports
var express                 = require('express'),
    router                  = express.Router(),
    basicRoutes             = require('./routes/basic'),
    collectionsRoutes       = require('./routes/collections');

// this module needs context
module.exports = function () { 
    var self = this; // cache the app context
    
    // Attaching routes
    [
        basicRoutes,
        collectionsRoutes
    ].forEach(function (route) {
        route.call(self, router);
    })
    
    return router;
};