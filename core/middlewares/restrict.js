// Middleware for restricted areas
'use strict';

module.exports = function (req, res, next) {
    if (req.session.user) {
        next();
    } else {
        req.session.error = 'Access denied!';
        console.log('Admin app:', req.ip, ' > tried to access: ', req.path);
        res.redirect(this.url + '/login');
    }
};