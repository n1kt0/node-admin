'use strict';

// imports
var Promise     = require('bluebird'),
    MongoDB     = Promise.promisifyAll(require('mongodb')),
    hash        = Promise.promisify(require('./pass'));

module.exports = {
    // Fn to establish db collection
    connectDB: function (server, port, dbname) {
        if (this.db !== null) return console.log('Admin App: DB already connected.');
        var uri = 'mongodb://' + server + ':' + port + '/' + dbname,
            self = this; // cachec the app context
        
        MongoDB.MongoClient.connectAsync(uri)
        .then(function (db) {
            console.log('Admin app: DB connection established.');
            self.db = db;
            self.emit('dbAssigned', self.db);
        })
        .catch(function(e){
            console.log(new Error(e));
        });
    },
    
    // Fn to check if admin users collection is fetchable and create own if not
    usersCheck: function (db) {
        var adminUsersCollection = 'admin_mod-users';
        
        db.collectionAsync(adminUsersCollection, {strict: true})
        .then(function (adminUsers) { // users collection exists
            console.log('Admin app: Admin users fetched');
        })
        .catch(function (e) { // users collection not created yet
            console.log(e.toString()); // inform of it
            var adminUsers;
            
            db.createCollectionAsync(adminUsersCollection) // create it
            .then(function (collection) {
                adminUsers = collection;
                return hash('admin')
            })
            .then(function (hashData) {
                var salt = hashData[0], hash = hashData[1];
                adminUsers.insert({ // populate it with stock admin account
                    name: 'admin',
                    salt: salt,
                    hash: hash
                }, { w: 0 });
                console.log('Admin app:', adminUsersCollection + ' created. Change admin\'s password.');
            }, function (e) {console.log(e);})
        });
    },
    
    settingsCheck: function (db) {
        var settingsCollection = 'admin_mod-settings';
        
        db.collectionAsync(settingsCollection, {strict: true})
        .then(function (settingsCollection) { // collection exists
            console.log('Admin app: Settings fetched');
        })
        .catch(function (e) { // collection doesn't exist. Create it
            console.log(e.toString());
            
            db.createCollectionAsync(settingsCollection)
            .then(function (collection) {
                console.log('Admin app: Settings Collection created.')
            })
        })
    }
}