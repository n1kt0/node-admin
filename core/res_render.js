var app_render = require('./app_render');

function res_render (view, options, fn){
    options = options || {};
    var self = this;
    var req = this.req;
    var app = req.app;

    // support callback function as second arg
    if ('function' == typeof options) {
        fn = options, options = {};
    }

    // merge res.locals
    options._locals = self.locals;

    // default callback to respond
    fn = fn || function(err, str){
        if (err) return req.next(err);
        self.send(str);
    };

    // render
    app_render.call(app, view, options, fn); // this was overwritten
}

module.exports = res_render