var gulp        = require('gulp'),
    stylus      = require('gulp-stylus'),
    minifyCSS   = require('gulp-minify-css'),
    rename      = require('gulp-rename'),
    browserify  = require('gulp-browserify'),
    notify      = require('gulp-notify'),
    hbsfy       = require('hbsfy'),
    uglify      = require('gulp-uglify'),
    watch       = require('gulp-watch'),
    concat      = require('gulp-concat');

gulp.task('stylus', function () {
    return gulp.src('core/frontend/stylus/*.styl')
        .pipe(stylus())
        .pipe(gulp.dest('dist/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifyCSS({keepBreaks:true}))
        .pipe(gulp.dest('dist/css'));
        //.pipe(notify({message: 'Sir! Stylus task complete, sir!'}));
});

gulp.task('browserify', function () {
    return gulp.src('core/frontend/js/app.js')
        .pipe(browserify({
            insertGlobals : true,
            transform: [hbsfy]
        }))
        .pipe(rename({suffix: '.bundle'}))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('extLibs', function () {
    return gulp.src('core/frontend/js/libs/*.js')
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('watch', function () {
    watch('core/frontend/js/**/*.js', function () {
        gulp.start('browserify');
    });
    watch('core/frontend/stylus/*.styl', function () {
        gulp.start('stylus');
    });
    watch('core/frontend/js/templates/**/*.hbs', function () {
        gulp.start('browserify');
    })
})

gulp.task('default', ['stylus', 'browserify', 'extLibs'], function () {});