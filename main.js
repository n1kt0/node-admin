 'use strict';

/* DEPENDENCIES*/

// external
var express         = require('express'),
    util            = require('util'),
    EventEmitter    = require('events').EventEmitter,
    session         = require('express-session'),
    cookieParser    = require('cookie-parser'),
    bodyParser      = require('body-parser'),
    Promise         = require('bluebird'),

//internal
    router          = require('./core/router'),
    connectDB       = require('./core/db').connectDB,
    usersCheck      = require('./core/db').usersCheck,
    settingsCheck   = require('./core/db').settingsCheck;

/* CONSTRUCTOR*/

function Admin(initData) {
    // instance props
    this.app = initData.app; // cache the master app
    this.url = initData.adminUrl[0] == '/' ? initData.adminUrl : '/' + initData.adminUrl;
    this.db = initData.db || null;
    
    // instance methods
    this.connectDB = connectDB.bind(this);
    
    // Middlewares:
    // admin front-end statics middleware
    this.app.use(this.url, express.static(__dirname + '/dist'));
    // admin session management middleware
    this.app.use(this.url, session({ secret: '3jfv0(kasdjkA*&^%5634hhdsf1)*hn_kkfH@ld+', key: 'adid', resave: true, saveUninitialized: true}));
    // admin body parser middleware
    this.app.use(this.url, bodyParser.json()); // body parser for API
    this.app.use(this.url + '/login', bodyParser.urlencoded({extended: true})); // body parser for login form
    // admin cookie parser middleware
    this.app.use(this.url, cookieParser());
    
    // init routes
    this.app.use(this.url, router.call(this));
    
    // check/create module users collection if/when db is assigned
    if (this.db) {
        // if we were supplied with db instance, promisify it
        this.db = Promise.promisifyAll(this.db);
        // then run check on it
        usersCheck.call(this, this.db);
        settingsCheck.call(this, this.db);
    } else {
        // otherwise tie event
        this.once('dbAssigned', usersCheck.bind(this));
        this.once('dbAssigned', settingsCheck.bind(this));
    }
    
    this.once('initialized', this.onceInitialized);
    this.emit('initialized', this);
}

// Inherit EvenEmitter API
util.inherits(Admin, EventEmitter);

/* METHODS*/

Admin.prototype.onceInitialized = function (adminApp) {
    console.log('Admin app: Initialization complete.');
}

/* EXPORT*/

module.exports = Admin;